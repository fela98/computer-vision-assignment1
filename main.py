import cv2 as cv
import matplotlib.pyplot as plt

img = cv.imread('object.png', 0)

# Otsu's thresholding
ret, th = cv.threshold(img,0,255,cv.THRESH_BINARY+cv.THRESH_OTSU)
# Adaptive tresholding
# th = cv.adaptiveThreshold(img,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,31,4)

plt.imshow(th, plt.cm.gray)
plt.axis('off')
plt.show()